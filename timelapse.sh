#!/bin/sh

hours=1

while getopts h: opt
do
   case $opt in
       h) hours=$OPTARG
       ;;
   esac
done

minutes=$((hours*60))

filenumb=1

mkdir /home/pi/timelapse

for i in $(find /home/pi/NaturewatchCameraServer/naturewatch_camera_server/static/data/photos/2*.jpg -mmin -${minutes}); do # Not recommended, will break on whitespace
	cp $i /home/pi/timelapse/$(printf img-%09d.%s ${filenumb%.*} ${i##*.})
	filenumb=$((filenumb+1))	
done

formattedDate=`date +%F`

ffmpeg -framerate 30 -i /home/pi/timelapse/img-%09d.jpg -s:v 1920x1080 -c:v libx264 -crf 17 -pix_fmt yuv420p /home/pi/timelapse-${hours}-${formattedDate}.mp4

thumb=`ls /home/pi/timelapse/ |sort -R | tail -10 | head -n 1`

mv -f /home/pi/timelapse-${hours}-${formattedDate}.mp4 /home/pi/NaturewatchCameraServer/naturewatch_camera_server/static/data/videos/
mv -f /home/pi/timelapse/${thumb} /home/pi/NaturewatchCameraServer/naturewatch_camera_server/static/data/videos/thumb_timelapse-${hours}-${formattedDate}.jpg

rm -rf /home/pi/timelapse
